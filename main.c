#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char getRandomCharacter();
void memory();
void printMenuSystem();

int main()
{
	printf("\n\n           MEMORY VIRTUALIZATION PROGRAM        \n-------------------------------------------------\n              OPERATING SYSTEMS CA\n-------------------------------------------------\n            KEELAN FAUL  -  D00165552\n-------------------------------------------------\n\n");
	memory();
	return 0;
}
void printMenuSystem(){
		printf("\n\nType a number to select an option\n 1. Show Physical Memory \n 2. Show Page Table\n 3. Search Virtual Address\n 4. Exit\n");
}

char getRandomCharacter(){
	 
	int randomNum = (rand() % (126 - 33 + 1)) + 33;
	char randomCharacter = randomNum;
	return randomCharacter;
}

void memory(){
	char * memorySpace = malloc(65536);
    FILE * memoryFile = fopen("data/physical_memory.txt", "w");
    FILE * pageTableFile = fopen("data/page_table.txt", "w");
    unsigned int offset_mask = 0x00FF;

	srand(time(0));
	int randomNumberBytes = (rand() % (20480 - 2048 + 1)) + 2048;
	int PAGE_SIZE = 256;
	int amountOfFrames = randomNumberBytes / PAGE_SIZE;
	int enteredAddress;
	int y = 2;
	
	for (int i = 512; i < randomNumberBytes; ++i){
		memorySpace[i] = getRandomCharacter();
	}

	//Printing page_table.txt 
	int x = 2;
	fprintf(pageTableFile, "|      Page     |   Page Table Entry   \n|---------------|------------------------------------\n");				 
	for(int i = 0; i < amountOfFrames; i++){
		memorySpace[i] = x;
		memorySpace[i + PAGE_SIZE] = 1;
		fprintf(pageTableFile, "|      0x%x      |      %d       \n", i, memorySpace[i]);
		x += 1;
    }

    //Printing to physical_memory.txt
    fprintf(memoryFile, "|    Address      |   Content   |\n|-----------------|-------------|\n");
    for(int i = 512; i < 65536; i++){

        fprintf(memoryFile, "|      0x%x      |      %c      |\n",i, memorySpace[i]);

    }
	int choice;
	do{
		printMenuSystem();
		scanf("%d", &choice);
		switch(choice){
			case 1:
				 printf("|    Address      |   Content   |\n|-----------------|-------------|\n");
    				for(int i = 512; i < 65536; i++){      		
        					if( memorySpace == NULL ) {
  									printf("|      0x%x      |      %c      |\n",i, memorySpace[i]);
 							 }else{
  									printf("|      0x%x      |  Not Used    |\n",i);
 							}
  					 }
			break;
			case 2:
				
				printf("|      Page     |   Page Table Entry   \n|---------------|------------------------------------\n");				 
					for(int i = 0; i < amountOfFrames; i++){
							memorySpace[i] = y;
							memorySpace[i + PAGE_SIZE] = 1;
							printf("|      0x%x      |      %d       \n", i, memorySpace[i]);
							y += 1;
    				}		
			break;
			case 3:

    			printf("Enter a memory Address in Hexadecimal form:");
  	 			scanf("%x", &enteredAddress);
    			unsigned int VPN = enteredAddress >> 8;
    			unsigned int offset = enteredAddress & offset_mask;

    			printf("Address - 0x%x \nVPN - 0x%x \nOffset - 0x%x \n", enteredAddress,VPN,offset);

    			int PFN = memorySpace[VPN];
    			unsigned int foundAddress = PFN << 8;
    			foundAddress |= offset;

    			printf("PFN %d\n", PFN);
    			printf("Character Found:  %c\n\n\n",memorySpace[foundAddress]);
				

			break;
			default:

			printf("\nInvalid Input, Try Again");
			break;
		}
	}while(choice != 4);

		printf("\n\nEXITING PROGRAM GOODBYE!!!!!");
	 fclose(pageTableFile);
	 fclose(memoryFile);
     free(memorySpace);

}